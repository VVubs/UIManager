// Quinn Daggett - 100618734
#include "Plugin.h"

Plugin::Plugin()
{

}

Plugin::~Plugin()
{

}

// Used for marshalling C#
void Plugin::ChangeState(int newState)
{
	state = newState;
}

// Load file function uses function Kyle's DLL tutorial as a base, modified to read entire contents of file with while loop
void Plugin::LoadFile(char * fileName)
{
	// Defining the file location for easy access
	std::string location;
	std::string name(fileName);
	location = name + ".txt";
	std::ifstream textFile;
	std::cout << fileName << std::endl;

	// Opening the text file and reading its contents
	textFile.open(location, std::ios::in);
	if (textFile.is_open())
	{
		while (std::getline(textFile, temp))
		{
			dataPack.push_back(temp); // Contents stored in dataPack vector
		}
	}
	else
	{
		std::cout << "File not found" << std::endl;
	}
	textFile.close();
}

void Plugin::Compose(int index)
{
	// Composing the strings from datapack into an element
	tempElement.name = dataPack[0 + (index * 10)];
	tempElement.text = dataPack[1 + (index * 10)];
	tempElement.xPos = std::stof(dataPack[2 + (index * 10)]);
	tempElement.yPos = std::stof(dataPack[3 + (index * 10)]);
	tempElement.zPos = std::stof(dataPack[4 + (index * 10)]);
	tempElement.red = std::stof(dataPack[5 + (index * 10)]);
	tempElement.green = std::stof(dataPack[6 + (index * 10)]);
	tempElement.blue = std::stof(dataPack[7 + (index * 10)]);
	tempElement.alpha = std::stof(dataPack[8 + (index * 10)]);
	tempElement.fontSize = std::stoi(dataPack[9 + (index * 10)]);
}

// Return functions
// DLL-friendly string return function uses function from Kyle's DLL tutorial as a base, modified to interface with tempElement 
char * Plugin::ReturnName(int newState)
{
	// C++ must move strings to C# as an array of chars
	char* temp2 = new char[tempElement.name.length() + 1]; // Create charpointer
	std::strcpy(temp2, tempElement.name.c_str()); // Copy the string as an array of chars
	text = temp2; // Set the array of chars to the return value
	return text; // Return it
}

char * Plugin::ReturnText(int newState)
{
	char* temp2 = new char[tempElement.text.length() + 1];
	std::strcpy(temp2, tempElement.text.c_str());
	text = temp2;
	return text;
}

float Plugin::ReturnPosX()
{
	return tempElement.xPos;
}

float Plugin::ReturnPosY()
{
	return tempElement.yPos;
}

float Plugin::ReturnPosZ()
{
	return tempElement.zPos;
}

float Plugin::ReturnRed()
{
	return tempElement.red;
}

float Plugin::ReturnGreen()
{
	return tempElement.green;
}

float Plugin::ReturnBlue()
{
	return tempElement.blue;
}

float Plugin::ReturnAlpha()
{
	return tempElement.alpha;
}

int Plugin::ReturnFontSize()
{
	return tempElement.fontSize;
}

