// Quinn Daggett - 100618734
#ifndef PLUGIN_H
#define PLUGIN_H

#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <stdlib.h>
#include <iostream>
#include "LibSettings.h"
#include <fstream>
#include <string>
#include <vector>

// Struct used for exporting UI element properties
struct LIB_API Element
{
	std::string name;
	std::string text;
	float xPos, yPos, zPos;
	float red, green, blue, alpha;
	int fontSize;
};

class LIB_API Plugin
{
public:
	// Data members
	int state = 0; // Used for marshalling C#
	std::string temp; // Temporary string used for reading from text file
	std::vector <std::string> dataPack; // Vector of strings used to store data read from text file via temp string
	Element tempElement; // Element used for composing data from string vector for exporting
	char* text; // Used when composing strings from array of chars
	Plugin(); // Constructor (does nothing)
	~Plugin(); // Destructor (does nothing)

	// Functions
	void ChangeState(int newState); // Used for marshalling C#
	void LoadFile(char* fileName); // Opens a text file and stores its contents
	void Compose(int index); // Composes data into an Element struct from dataPack vector
	char* ReturnName(int newState); // Getter for accessing name
	char* ReturnText(int newState); // Getter for accessing text
	float ReturnPosX(); // Getter for accessing x position
	float ReturnPosY(); // Getter for accessing y position
	float ReturnPosZ(); // Getter for accessing z position
	float ReturnRed(); // Getter for accessing r value
	float ReturnGreen(); // Getter for accessing g value
	float ReturnBlue(); // Getter for accessing b value
	float ReturnAlpha(); // Getter for accessing alpha
	int ReturnFontSize(); // Getter for accessing fontsize


};

#endif
