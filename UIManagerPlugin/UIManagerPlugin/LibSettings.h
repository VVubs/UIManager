// Quinn Daggett - 100618734
// Code for dll
#ifndef __LibSettings_h
#define __LibSettings_h

#ifdef PLUGIN_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#ifdef PLUGIN_IMPORTS
#define LIB_API __declspec(dllimport)
#else
#define LIB_API
#endif
#endif
#endif

