// Quinn Daggett - 100618734
#ifndef __Wrapper_h
#define __Wrapper_h
#include "LibSettings.h"
#include "Plugin.h"

#ifdef __cplusplus
extern "C"
{
#endif

	void LIB_API LoadDLL();
	void LIB_API ChangeState(int newState);
	void LIB_API LoadFile(char* fileName);
	void LIB_API Compose(int index);
	char LIB_API* ReturnName(int newState);
	char LIB_API* ReturnText(int newState);
	float LIB_API ReturnPosX();
	float LIB_API ReturnPosY();
	float LIB_API ReturnPosZ();
	float LIB_API ReturnRed();
	float LIB_API ReturnGreen();
	float LIB_API ReturnBlue();
	float LIB_API ReturnAlpha();
	int LIB_API ReturnFontSize();


#ifdef __cplusplus
};

#endif
#endif
