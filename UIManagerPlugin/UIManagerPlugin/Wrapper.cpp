// Quinn Daggett - 100618734
#include "Wrapper.h"

Plugin* instance;

void LIB_API LoadDLL()
{
	instance = new Plugin();
}

void LIB_API ChangeState(int newState)
{
	instance->ChangeState(newState);
}

void LIB_API LoadFile(char * fileName)
{
	instance->LoadFile(fileName);
}

void LIB_API Compose(int index)
{
	instance->Compose(index);
}

char LIB_API * ReturnName(int newState)
{
	return instance->ReturnName(newState);
}

char LIB_API * ReturnText(int newState)
{
	return instance->ReturnText(newState);
}

float LIB_API ReturnPosX()
{
	return instance->ReturnPosX();
}

float LIB_API ReturnPosY()
{
	return instance->ReturnPosY();
}

float LIB_API ReturnPosZ()
{
	return instance->ReturnPosZ();
}

float LIB_API ReturnRed()
{
	return instance->ReturnRed();
}

float LIB_API ReturnGreen()
{
	return instance->ReturnGreen();
}

float LIB_API ReturnBlue()
{
	return instance->ReturnBlue();
}

float LIB_API ReturnAlpha()
{
	return instance->ReturnAlpha();
}

int LIB_API ReturnFontSize()
{
	return instance->ReturnFontSize();
}
